<?php
//functions

function csrf_get() {
	return md5(session_id().date("H"));
}
function prop_form_csrf() {
	?><input type="hidden" name="csrf" value="<?=csrf_get();?>"><?
}
function csrf_check($v="") {
	if($v===md5(session_id().date("H")) || $v===md5(session_id().date("H",(time()-3600)))) {
		return true;
	}
	return false;
}

function _n() {
	$a = func_get_args();
	foreach($a as $v) {
		if(trim($v!="")) {
			return trim($v);
		}
	}
	return "";
}

function yatranslite($s="") {
		$A = array();
		$A["Ё"]="YO";$A["Й"]="I";$A["Ц"]="TS";$A["У"]="U";$A["К"]="K";$A["Е"]="E";$A["Н"]="N";$A["Г"]="G";$A["Ш"]="SH";$A["Щ"]="SCH";$A["З"]="Z";$A["Х"]="H";$A["Ъ"]="";
		$A["ё"]="yo";$A["й"]="i";$A["ц"]="ts";$A["у"]="u";$A["к"]="k";$A["е"]="e";$A["н"]="n";$A["г"]="g";$A["ш"]="sh";$A["щ"]="sch";$A["з"]="z";$A["х"]="h";$A["ъ"]="";
		$A["Ф"]="F";$A["Ы"]="I";$A["В"]="V";$A["А"]="A";$A["П"]="P";$A["Р"]="R";$A["О"]="O";$A["Л"]="L";$A["Д"]="D";$A["Ж"]="ZH";$A["Э"]="E";
		$A["ф"]="f";$A["ы"]="i";$A["в"]="v";$A["а"]="a";$A["п"]="p";$A["р"]="r";$A["о"]="o";$A["л"]="l";$A["д"]="d";$A["ж"]="zh";$A["э"]="e";
		$A["Я"]="YA";$A["Ч"]="CH";$A["С"]="S";$A["М"]="M";$A["И"]="I";$A["Т"]="T";$A["Ь"]="";$A["Б"]="B";$A["Ю"]="YU";
		$A["я"]="ya";$A["ч"]="ch";$A["с"]="s";$A["м"]="m";$A["и"]="i";$A["т"]="t";$A["ь"]="";$A["б"]="b";$A["ю"]="yu";
		$A[" "]="-";$A[","]="";$A["/"]="";$A["."]="-";
		$s = strtr($s,$A);
		return preg_replace("/[^\x20-\x7F]/", "", $s);
}

function getmicrotime() {list($usec,$sec) = explode(" ",microtime());return ((float)$usec + (float)$sec);};


function saferead($file) {
	$buffer = "";
	ignore_user_abort(1);
	if(is_readable($file)) {
		$fp = fopen($file, "rb");
		if($fp) {
			$count = 100;
			$lockbool=true;
			while ($lockbool && $count>0) { 
				$lockbool=!flock($fp, (LOCK_SH+LOCK_NB)); 
				$count--;
			}
			if (!$lockbool) { 
					while (!feof($fp)) {
					  $buffer .= fread($fp, 2048); 
					}
			    flock($fp, LOCK_UN); 
			} else {
					while (!feof($fp)) {
					  $buffer .= fread($fp, 512);
					}
			}
			fclose($fp);
		} else {
			return false;
		}
	} else {
		return false;
	}
	ignore_user_abort(0);
	return $buffer;
}

function safewrite($file, $data="") {
	ignore_user_abort(1);
	if(trim($file)!="" && (!file_exists($file) || is_writable($file))) {
		if(!file_exists($file)) {
			$t = fopen($file, "w");
			fclose($t);
		}
		$fp = fopen($file, "r+b");
		if($fp) {
			$count = 100;
			$lockbool=true;
			while ($lockbool && $count>0) {
				$lockbool=!flock($fp, LOCK_EX);
				$count--;
			}
			if (!$lockbool) {
			   fwrite($fp, $data);
			   ftruncate($fp,strlen($data));
			   flock($fp, LOCK_UN);
			} else {
				$salt = md5(uniqid(rand(), true));
				fclose($fp);
				$fp = fopen($file.$salt,"w+b");
			  fwrite($fp, $data);
				fclose($fp);
				rename($file,$file.$salt.".chk");
				rename($file.$salt,$file);
				if(file_exists($file)) {
					@unlink($file.$salt.".chk");
					return true;
				} else {
					@rename($file.$salt.".chk",$file);
					return false;
				}
			}
			fclose($fp);
		} else {
			return false;
		}
	} else {
		return false;
	}
	ignore_user_abort(0);
	return true;
}

//Slashify - make sure path ends in a slash
function slashify($path) {
    if ($path[strlen($path)-1] != '/') {
        $path = $path."/";
    }
    return $path;
}

//Unslashify - make sure path doesn't in a slash
function unslashify($path) {
    if ($path[strlen($path)-1] == '/') {
        $path = substr($path, 0, (strlen($path)-1) );
    }
    return $path;
}

//Merge two pathes, make sure there is exactly one slash between them
function mergepathes($parent, $child) 
{
    if ($child{0} == '/') {
        return unslashify($parent).$child;
    } else {
        return slashify($parent).$child;
    }
}

function customsort($a,$cmd) {
	global $_customsort_cache;
	$dims = array(); $fname = "";
	$d = explode(",",$cmd);
	foreach($d as $dim) {
		$t = explode(" ",trim($dim));
		if(trim($t[0])) {
			$dims[] = array("field"=>$t[0],"desc"=>(isset($t[1])?(trim($t[1])=="desc"?1:0):0),"num"=>(isset($t[2])?($t[2]=="num"?1:0):0));
		}//end if
	}
	if(count($dims)) {
		if(isset($_customsort_cache) && is_array($_customsort_cache) && array_key_exists(md5(serialize($dims)),$_customsort_cache)) {
			$fname = $_customsort_cache[md5(serialize($dims))];
		} else {
			$fbody = 'return ( $$$next );';
			foreach($dims as $key=>$item) {
				if($item["num"]) {
					$tpl = '($a$$$dim==$b$$$dim? ( $$$next ) :($a$$$dim>$b$$$dim?$$$out))';
				} else {
					$tpl = '(strcasecmp($a$$$dim,$b$$$dim)==0? ( $$$next ) :(strcasecmp($a$$$dim,$b$$$dim)>0?$$$out))';
				}//end if
				$tpl = str_replace('$$$dim',$item["field"],$tpl);
				$tpl = str_replace('$$$out',($item["desc"]?"-1:1":"1:-1"),$tpl);
				$fbody = str_replace('$$$next',$tpl,$fbody);
			}//end fe
			$fbody = str_replace('$$$next',"0",$fbody);
			if(!isset($_customsort_cache) || !is_array($_customsort_cache)) {
				$_customsort_cache = array();
			}
			$_customsort_cache[md5(serialize($dims))] = create_function('$a,$b',$fbody);
		}//end if
		if(uasort($a,$_customsort_cache[md5(serialize($dims))])) {
			return $a;
		} else {
			return false;
		}//end if
	} else {
		return $a;
	}//end if
}//end func customsort

function translate($val,$fmin,$fmax,$tmin,$tmax) {
	if($val<$fmin) $val = $fmin;
	if($val>$fmax) $val = $fmax;
	$shift = ($val-$fmin)/($fmax-$fmin);
	return round($tmin+($shift*($tmax-$tmin)));
}

function hsv2rgb($h, $s, $v) 
{
    $s /= 256.0;
    if ($s == 0.0) return array($v,$v,$v);
    $h /= (256.0 / 6.0);
    $i = floor($h);
    $f = $h - $i;
    $p = (integer)($v * (1.0 - $s));
    $q = (integer)($v * (1.0 - $s * $f));
    $t = (integer)($v * (1.0 - $s * (1.0 - $f)));
    switch($i) {
    case 0: return array($v,$t,$p);
    case 1: return array($q,$v,$p);
    case 2: return array($p,$v,$t);
    case 3: return array($p,$q,$v);
    case 4: return array($t,$p,$v);
    default: return array($v,$p,$q);
    }
}

function rgb2html($r, $g=-1, $b=-1)
{
    if (is_array($r) && sizeof($r) == 3)
        list($r, $g, $b) = $r;

    $r = intval($r); $g = intval($g);
    $b = intval($b);

    $r = dechex($r<0?0:($r>255?255:$r));
    $g = dechex($g<0?0:($g>255?255:$g));
    $b = dechex($b<0?0:($b>255?255:$b));

    $color = (strlen($r) < 2?'0':'').$r;
    $color .= (strlen($g) < 2?'0':'').$g;
    $color .= (strlen($b) < 2?'0':'').$b;
    return '#'.$color;
}

?>