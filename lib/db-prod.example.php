<?

ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory("./models");
    $cfg->set_connections(array('dev' => 'mysql://user:pass@127.0.0.1/dbname'));
    $cfg->set_default_connection('dev');
});

$s = Setting::find('all');
$_['s'] = array();
if(!empty($s)) {
	foreach($s as $v) {
		$_['s'][$v->id] = $v->val;
	}
}

?>