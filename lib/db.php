<?

ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory("./models");
    $cfg->set_connections(array('dev' => 'mysql://root:@127.0.0.1/spark-base'));
    $cfg->set_default_connection('dev');
});

try{
	$s = Setting::find('all');
	$_['s'] = array();
	if(!empty($s)) {
		foreach($s as $v) {
			$_['s'][$v->id] = $v->val;
		}
	}
} catch(Exception $e) {
	ActiveRecord\Config::instance()->set_connections(array());
	if(empty($_SESSION["flash"])) {
		$_SESSION["flash"] = array();
	}
	$msg = explode("\n",$e->getMessage()); $msg = $msg[0];
	$_SESSION["flash"] = array('db'=>'DB problem: '.$msg);
}

?>