<?php

class User extends ActiveRecord\Model
{
	static $attr_protected = array('id');

	static $validates_size_of = array(
		array("name", "within"=>array(1,50), "too_short"=>"Не указано имя!","too_long"=>"Слишком длинно!"),
		array("login", "within"=>array(1,50), "too_short"=>"Не указан логин!", "too_long"=>"Слишком длинно!"),
		array("pass", "within"=>array(1,50), "too_short"=>"Не задан пароль!", "too_long"=>"Слишком длинно!"),
	);
    static $validates_format_of = array(
		array('login', 'with' =>
		"/^[a-zA-Z0-9-_]*$/", "message" => "Неверный формат (латинские буквы и цифры, без пробелов)")
	);
	
	/*static $has_many = array(
		//array('objclass-plural'),
	);*/

	
}

?>