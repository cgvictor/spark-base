<?php
//login user via post data

if(!empty($_POST["login"]) && !empty($_POST["password"])) {
	$user = User::find_by_login($_POST["login"]);
	if($user) {
		if($user->pass == md5($user->salt.$_POST["password"])) {
			$_SESSION["user"] = $user->login;
			session_write_close();
			if(!empty($_REQUEST["redirect"])) header("Location: ".$_REQUEST["redirect"]);
				else header("Location: /");
		} else {
			flash("Неверный пароль");
		}
	} else {
		flash("Пользователь не найден");
	}
	if(!empty($_REQUEST["redirect"])) header("Location: ".$_REQUEST["redirect"]);
	else header("Location: /");
	session_write_close(); exit;
} else {
	flash("Данные не переданы");
	header("Location: /");
	exit;
}

?>