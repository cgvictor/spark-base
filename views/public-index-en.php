﻿<?php
//home screen
//$_t["title"] = $_SERVER["HTTP_HOST"];
include("./views/common/_head.php");

?>
    <nav class="navbar" style="margin-bottom:7px">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><?=!empty($_["s"]["global.sitename"])?$_["s"]["global.sitename"]:'Baseband Spark Install (static)';?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<? if($dbenabled) {include('./views/common/_authform.php');}; ?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="jumbotron">
      <div class="container">
		<div style="margin-top:10px">
			<a href="/" class="btn btn-link">Russian</a>
			<a href="/en" class="btn btn-default">English</a>
		</div>
        <h1 style="display:none">Baseband Spark</h1>
		<img src="/i/spark.png">
        <p>This is a clean install of the Spark web router & engine, published under Baseband product family. The engine was created to host fast web experiments, but is capable to serve large projects as well. Now it's all clear to start working.</p>
        <p><a class="btn btn-default btn-lg" href="https://bitbucket.org/cgvictor/spark-base/" role="button">Spark repository &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h2><small><span class="glyphicon glyphicon-home" aria-hidden="true"></span></small> Getting started</h2>
          <p>Public program code is located in <code>/public</code>. For instance, this page is served by <code>/public/en</code>. Treat this as a <u>controller</u> under MVC terms.</p>
		  <p>To render this page we use markup from <code>/views/public-index-en</code> - this acts as MVC-<u>view</u> with your HTML code.</p>
		  <p><b>Go create</b> an example.php in <code>/public</code>, and it will become accessible by <code>http://<?=$_SERVER["HTTP_HOST"];?>/example</code>.</p>
        </div>
        <div class="col-md-4">
		  <? if(!empty($dbenabled)) { ?>
			  <h2><small><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></small> Working with DB</h2>
			  <p>DB is connected sucessfully, ORM&nbsp;<a href="http://www.phpactiverecord.org/projects/main/wiki/Basic_CRUD">PHP ActiveRecord</a> is&nbsp;connected now out the box.</p>
			  <p>Model classes (MVC-<u>models</u>) are located under <code>/models</code>. Create necessary tables in your DB, copy model file and start using DB ORM object.</p>
			  <p>If you had used the DB dump from <code>db.sql</code> then you can login as <code>admin:sparkadmin</code>.</p>
			  <p><a class="btn btn-default" href="http://www.phpactiverecord.org/projects/main/wiki/Basic_CRUD" role="button">PHP ActiveRecord docs &raquo;</a></p>
		  <? } else { ?>
			  <h2><small><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></small> DB not connected</h2>
			  <p>To connect DB to your project:</p>
			  <ul>
				<li>Uncomment in <code>index.php:30</code> ORM connection code block</li>
				<li>Set up inside <code>./lib/db.php</code> your connection creds (user-pass-dbname)</li>
				<li>Check for connection errors (if any, on top) by&nbsp;refreshing this page</li>
			  </ul>
			  <p>DB dump example is in <code>db.sql</code>. User auth example will work with it as well.</p>
			  <p></p>
		  <? } ?>
       </div>
        <div class="col-md-4">
          <h2><small><span class="glyphicon glyphicon-gift" aria-hidden="true"></span></small> License</h2>
          <p>Spark uses dual license options, up to you to choose which suits best your needs</p>
		  <ul>
			<li>CC <a href="http://creativecommons.org/licenses/by-nc/4.0/">v4 Attribution NonCommercial</a></li>
			<li><a href="https://opensource.org/licenses/MIT">MIT License</a></li>
		  </ul>
		  <p>If you like Spark or made a good use of it, please consider placing a weblink pointing to our project. However, this is not mandatory.</p>
          <p><a class="btn btn-default" href="http://basebandcc.ru/" role="button">Baseband product line&raquo;</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; <?=date('Y');?> <?=$_SERVER["HTTP_HOST"];?>
		<span style="color:#ccc">/ <?=round((getmicrotime()-$_["time"]["s"]),3);?>&nbsp;ms <?=round(memory_get_peak_usage()/1000);?> kb</span></p>
      </footer>
    </div> <!-- /container -->

<? 
include("./views/common/_foot.php");
?>