<?php
//common head
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=0.7">
<title><? if(!empty($_t["title"])) {
	?><?=$_t["title"];?> - <?=!empty($_["s"]["global.sitename"])?$_["s"]["global.sitename"]:'Baseband Spark Install (static)';?><?
} else {
	?><?=!empty($_["s"]["global.sitename"])?$_["s"]["global.sitename"]:'Baseband Spark Install (static)';?><?
}?></title>
	<link rel="icon" type="image/x-icon" href="/s16.ico" />
	<link rel="stylesheet" type="text/css" href="/i/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/i/style.css" />
    <script type="text/javascript" src="/i/jquery-2.2.1.min.js"></script>
	<script type="text/javascript" src="/i/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/i/js.js"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<?
	if(!empty($_SESSION["flash"]) && count($_SESSION["flash"])>0) {
		?><div class="container flash"><?
		foreach($_SESSION["flash"] as $k=>$v) {
			?><div class="col-xs-12 alert alert-warning flash-item"><?=htmlspecialchars($v);?></div><?
		}
		?><div class="clear"></div></div><?
		$_SESSION["flash"] = array();
	}
	?>
<?php ?>