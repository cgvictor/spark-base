<?php 
$_["time"]["p3"] = getmicrotime()-$_["time"]["s"];

$r = parse_url(_n($_SERVER["HTTP_REFERER"]));
$fi = pathinfo($_["m"]);
$stat = array(
	round($_["time"]["s"],3),
	_n($_["snippet"]),
	_n($_["request"][0]),
	_n($fi["filename"]),
	round($_["time"]["p1"],3),
	round($_["time"]["p2"],3),
	round($_["time"]["p3"],3),
	round(memory_get_peak_usage()/1000),
	(strpos($r["host"],$_SERVER["HTTP_HOST"])!==false)?"":substr($_SERVER["HTTP_REFERER"],0,512)
);

$stat_data = base64_encode(gzcompress(serialize($stat)));
$stat_hash = md5($stat_data."Y");

?>
	<!--
		<?=print_r($_["time"]);?>
	--></div><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  //ga('create', '', 'auto');
  //ga('send', 'pageview');

</script></body>
</html>