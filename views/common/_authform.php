<?php
//auth block
if($_["user"]) {
	$u = $_["user"];
	?>
	<div class="nav navbar-nav navbar-right navbar-text">
		Hello, <b><?=$u->name;?></b>!
		<a href="/logout" class="btn btn-xs btn-default">Log off</a>
	</div>
	<?php
} else {
	?>
	<form class="navbar-form navbar-right" action="/login" method="post">
		<div class="form-group">
			<input type="text" name="login" placeholder="Login" class="form-control">
		</div>
		<div class="form-group">
			<input type="password" name="password" placeholder="Password" class="form-control">
		</div>
		<button type="submit" class="btn btn-default">Sign in</button>
	</form>
	<?php
}
?>