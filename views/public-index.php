﻿<?php
//home screen
//$_t["title"] = $_SERVER["HTTP_HOST"];
include("./views/common/_head.php");

?>
    <nav class="navbar" style="margin-bottom:7px">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><?=!empty($_["s"]["global.sitename"])?$_["s"]["global.sitename"]:'Baseband Spark Install (static)';?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<? if($dbenabled) {include('./views/common/_authform.php');}; ?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="jumbotron">
      <div class="container">
		<div style="margin-top:10px">
			<a href="/" class="btn btn-default">Russian</a>
			<a href="/en" class="btn btn-link">English</a>
		</div>
        <h1 style="display:none">Baseband Spark</h1>
		<img src="/i/spark.png">
        <p>Это чистая установка роутера и движка Spark, семейство продуктов Baseband. Он создавался для разных быстрых экспериментов, хотя может поддерживать и большие проекты. Сейчас всё готово для того, чтобы начать работу.</p>
        <p><a class="btn btn-default btn-lg" href="https://bitbucket.org/cgvictor/spark-base/" role="button">Репозиторий Spark &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h2><small><span class="glyphicon glyphicon-home" aria-hidden="true"></span></small> С чего начать</h2>
          <p>Программный код, доступный без авторизации, лежит в <code>/public</code>. К&nbsp;примеру, эта страница отдается при помощи <code>/public/index</code>. Считайте, что это <u>контроллер</u> в&nbsp;терминах MVC.</p>
		  <p>Для отображения этой страницы используется разметка из <code>/views/public-index</code> - это MVC-<u>представление</u> с вашим html-кодом.</p>
		  <p><b>Создайте</b> в <code>/public</code> файл example.php, и он будет доступен по адресу <code>http://<?=$_SERVER["HTTP_HOST"];?>/example</code>.</p>
        </div>
        <div class="col-md-4">
		  <? if(!empty($dbenabled)) { ?>
			  <h2><small><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></small> Работа с данными</h2>
			  <p>База данных успешно подключена, используется&nbsp;ORM&nbsp;<a href="http://www.phpactiverecord.org/projects/main/wiki/Basic_CRUD">PHP ActiveRecord</a>.</p>
			  <p>Описания классов (<u>модели</u> в терминах MVC) хранятся в <code>/models</code>. Создайте необходимые таблицы в базе, скопируйте файл модели, и начните использовать базу.</p>
			  <p>Если вы использовали дамп базы данных из <code>db.sql</code>, то сможете войти на сайт с учетными данными <code>admin:sparkadmin</code>.</p>
			  <p><a class="btn btn-default" href="http://www.phpactiverecord.org/projects/main/wiki/Basic_CRUD" role="button">Документация по ActiveRecord &raquo;</a></p>
		  <? } else { ?>
			  <h2><small><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></small> БД не подключена</h2>
			  <p>Для подключения к базе:</p>
			  <ul>
				<li>Раскомментируйте в <code>index.php:30</code> строки подключения ORM</li>
				<li>Настройте в <code>./lib/db.php</code> данные строки подключения (логин-пароль-БД)</li>
				<li>Проверьте наличие ошибок подключения (вверху) при открытии этой страницы</li>
			  </ul>
			  <p>Пример дампа базы данных приведен в <code>db.sql</code>. На&nbsp;нем также будет работать пример авторизации.</p>
			  <p></p>
		  <? } ?>
       </div>
        <div class="col-md-4">
          <h2><small><span class="glyphicon glyphicon-gift" aria-hidden="true"></span></small> Лицензия</h2>
          <p>Spark поставляется по двойной лицензии, выберите наиболее подходящую вам.</p>
		  <ul>
			<li>CC <a href="http://creativecommons.org/licenses/by-nc/4.0/">v4 Attribution NonCommercial</a></li>
			<li><a href="https://opensource.org/licenses/MIT">MIT License</a></li>
		  </ul>
		  <p>Если Spark вам понравился или был полезен &mdash; пожалуйста, разместите активную ссылку на проект. Тем не менее, это не обязательно.</p>
          <p><a class="btn btn-default" href="http://basebandcc.ru/" role="button">Весь Baseband &raquo;</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; <?=date('Y');?> <?=$_SERVER["HTTP_HOST"];?>
		<span style="color:#ccc">/ <?=round((getmicrotime()-$_["time"]["s"]),3);?>&nbsp;ms <?=round(memory_get_peak_usage()/1000);?> kb</span></p>
      </footer>
    </div> <!-- /container -->

<? 
include("./views/common/_foot.php");
?>