![spark.png](https://bitbucket.org/repo/4d8Mdg/images/43787871-spark.png)

This is a home of Spark web engine and router, under Baseband product family.

### How do I get set up? ###

* Fork the code or get ZIP extracted into your web project folder
* Make changes to your server config, if necessary
* Open the host URL in browser
* If you need a DB, follow the instructions on main page
* The sample DB dump is in db.sql

### Contribution guidelines ###

We are currently not accepting contribution, since the project consists only of bare minimum for "real world" web engine. Fork this project, make all necessary enhancements and have your business logic implemented, after that we can consider merging some of your code achievements and practices into the basic repo.

### Who do I talk to? ###

* You always can contact me - **Victor Erofeev** - by email cg@cg.net.ru or any other way
* Check out our more mature products at [Baseband Content Control](http://basebandcc.ru/)