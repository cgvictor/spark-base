<?php
// Baseband Spark, MIT License / CC4 By-NC (your choice, what suits best). Educational purposes, no support.
// Victor Erofeev cg@cg.net.ru
// https://opensource.org/licenses/MIT
// http://creativecommons.org/licenses/by-nc/4.0/

Error_Reporting(E_ALL & ~E_NOTICE);
ini_set("session.use_only_cookies","1");
@setlocale(LC_ALL, 'ru_RU.UTF8');

if (get_magic_quotes_gpc()) { 
  strips($_GET); strips($_POST); strips($_FILES); strips($_COOKIE); strips($_REQUEST); 
  if (isset($_SERVER['PHP_AUTH_USER'])) strips($_SERVER['PHP_AUTH_USER']); 
  if (isset($_SERVER['PHP_AUTH_PW']))   strips($_SERVER['PHP_AUTH_PW']);
}

$r = explode("?",$_SERVER["REQUEST_URI"]);
$r0 = explode("/",$r[0]); $r1 = array(); foreach($r0 as $k=>$v) if(trim($v)!="") $r1[] = $v;
$_ = array(); $_["snippet"] = "index"; $_["scope"] = array("public"); $_["user"] = false;
if(count($r1)>0) $_["snippet"] = array_shift($r1); $_["request"] = $r1;

require_once("./lib/functions.php");
require_once("./lib/curlutil.php");

$_["time"] = array("s"=>getmicrotime());

session_set_cookie_params(1209600,'/','.'.$_SERVER["HTTP_HOST"],0,1);
session_start();
header("Content-type: text/html; charset=UTF-8");

// Uncomment these to enable DB connection

require_once "ActiveRecord.php";
if(file_exists("./lib/db-prod.php")) {
	include("./lib/db-prod.php");
} else {
	if(file_exists("./lib/db.php")) {
		include("./lib/db.php");
	}
}

if(!empty($_SESSION["user"])) {
	array_unshift($_["scope"], "user");
	$_["user"] = User::find_by_login($_SESSION["user"]);
	if(!empty($_["user"]) && $_["user"]->isadmin) {
		array_unshift($_["scope"], "user/admin");
	}
};

$_["time"]["p1"] = getmicrotime()-$_["time"]["s"];

$_["m"] = "null.php"; $_["m_use"] = true; $_t = array();
$_ok = false;
foreach($_["scope"] as $k=>$v) if(file_exists("./".$v."/".$_["snippet"].".php")) {
	include("./".$v."/".$_["snippet"].".php"); 
	$_ok = true;
	break;
}
if(!$_ok) {
	include("./views/404.php"); 
	break;
}
$_["time"]["p2"] = getmicrotime()-$_["time"]["s"];

if($_["m_use"] && file_exists("./views/".$_["m"])) {
	include("./views/".$_["m"]);
}
	
function strips(&$el) { 
  if (is_array($el)) 
    foreach($el as $k=>$v) 
      strips($el[$k]); 
  else $el = trim(stripslashes($el)); 
} 
function flash($msg) {
	if(empty($_SESSION["flash"])) $_SESSION["flash"] = array();
	$_SESSION["flash"][] = $msg;
}

//ob_start("ob_gzhandler");
?>